package com.xlrs.transaction.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Component;

import com.xlrs.commons.aspect.ThreadAttribute;
import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.transaction.feign.IllionProxy;
import com.xlrs.transaction.view.CustomerDataView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class IllionCustomerDataRestTemplate {
	
	
	@Autowired
//	private RestServiceBuilder<String> restServiceBuilder;
	private IllionProxy illionProxy;
	
	@Value("${customer.data.url}")
	private String getCustomerDataURL;
	
	@Autowired
	private MessageSource messages;

	public String getCustomerData(CustomerDataView customerDataView) throws NoSuchMessageException, ApplicationException {
		String customerDataResponse =null;
		try {
			if(getCustomerDataURL == null || "".equals(getCustomerDataURL)) {
				throw new ApplicationException(messages.getMessage("err.service.url.not.found", null, null));
			}
			Map<String,String> headers = new HashMap<String, String>();
			headers.put(CommonConstants.API_KEY, ThreadAttribute.getApiKey());
//			customerDataResponse = restServiceBuilder.postWithHeaders(getCustomerDataURL, customerDataView, headers, String.class);
			customerDataResponse = illionProxy.customerData(customerDataView, headers);
			log.info("Customer's data successfully featched from Illion System");
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.service.customerdata.error.message", null, null), e.getMessage(),e);
		}
		return customerDataResponse;
	}

}
