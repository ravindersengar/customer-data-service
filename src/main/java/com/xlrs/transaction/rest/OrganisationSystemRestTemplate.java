package com.xlrs.transaction.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.transaction.feign.OrganisationSystemProxy;
import com.xlrs.transaction.view.SystemView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class OrganisationSystemRestTemplate {
	
	
	@Autowired
//	private RestServiceBuilder<ResponseView> restServiceBuilder;
	private OrganisationSystemProxy organisationSystemProxy;
	
//	@Value("${organisationsystem.system.get.url}")
//	private String getOrganisationSystemURL;
	
	@Autowired
	private MessageSource messages;

	public SystemView getSystemByOrgSystemId(Long orgSystemId) throws ApplicationException {
		SystemView systemView =null;
		try {
//			if(getOrganisationSystemURL == null || "".equals(getOrganisationSystemURL)) {
//				throw new ApplicationException(messages.getMessage("err.service.url.not.found", null, null));
//			}
//			ResponseView responseView = restServiceBuilder.get(getOrganisationSystemURL, ResponseView.class, orgSystemId);
			ResponseView responseView = organisationSystemProxy.getOrganisationSystem(orgSystemId);
			if(responseView.getErrorMessages()==null) {
				systemView =  new ObjectMapper().convertValue(responseView.getData(), SystemView.class);
			}
			
			log.info("Getting system by organisation system id");
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.service.system.error.message", null, null), e.getMessage(),e);
		}
		return systemView;
	}

}
