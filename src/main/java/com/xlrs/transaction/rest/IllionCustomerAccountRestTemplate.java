package com.xlrs.transaction.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Component;

import com.xlrs.commons.aspect.ThreadAttribute;
import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.transaction.feign.IllionProxy;
import com.xlrs.transaction.view.CustomerAccountResponse;
import com.xlrs.transaction.view.CustomerAccountView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class IllionCustomerAccountRestTemplate {
	
	
	@Autowired
//	private RestServiceBuilder<CustomerAccountResponse> restServiceBuilder;
	private IllionProxy illionProxy;
	
	@Autowired
	private MessageSource messages;

	@Value("${customer.accounts.url}")
	private String getCustomerAccountURL;
	
	public CustomerAccountResponse getCustomerAccounts(CustomerAccountView customerAccountView) throws NoSuchMessageException, ApplicationException {
		CustomerAccountResponse customerAccountResponse =null;
		try {
			if(getCustomerAccountURL == null || "".equals(getCustomerAccountURL)) {
				throw new ApplicationException(messages.getMessage("err.service.url.not.found", null, null));
			}
			Map<String,String> headers = new HashMap<String, String>();
			headers.put(CommonConstants.API_KEY, ThreadAttribute.getApiKey());
//			customerAccountResponse = restServiceBuilder.postWithHeaders(getCustomerAccountURL, customerAccountView, headers, CustomerAccountResponse.class);
			customerAccountResponse = illionProxy.customerAccounts(customerAccountView, headers);
			if(customerAccountResponse==null || customerAccountResponse.getCustomer()==null) {
				log.error(messages.getMessage("err.customer.account.not.fetch", null, null), 
						customerAccountResponse.getErrorCode().concat(" - ").concat(customerAccountResponse.getErrorMessage()),null);
			}
			log.info("Customer's account successfully featched from Illion System");
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.service.customeraccountdata.error.message", null, null), e.getMessage(),e);
		}
		return customerAccountResponse;
	}

}
