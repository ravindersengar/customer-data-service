package com.xlrs.transaction.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.xlrs.commons.aspect.ThreadAttribute;
import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.transaction.feign.IllionProxy;
import com.xlrs.transaction.view.CreateCustomerResponse;
import com.xlrs.transaction.view.CreateCustomerView;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class IllionCreateCustomerRestTemplate {
	
	
	@Autowired
//	private RestServiceBuilder<CreateCustomerResponse> restServiceBuilder;
	private IllionProxy illionProxy;

	@Value("${customer.create.url}")
	private String createCustomerURL;
	
	@Autowired
	private MessageSource messages;
	
	public CreateCustomerResponse createCustomer(CreateCustomerView createCustomerView) throws ApplicationException {
		CreateCustomerResponse customerResponse =null;
		try { 
			if(createCustomerURL == null || "".equals(createCustomerURL)) {
				throw new ApplicationException(messages.getMessage("err.service.url.not.found", null, null));
			}
			Map<String,String> headers = new HashMap<String, String>();
			headers.put(CommonConstants.API_KEY, ThreadAttribute.getApiKey());
//			customerResponse = restServiceBuilder.postWithHeaders(createCustomerURL, createCustomerView, headers, CreateCustomerResponse.class);
			customerResponse = illionProxy.createCustomer(createCustomerView, headers);
			
			if(customerResponse==null || customerResponse.getEncryptionKey()==null) {
				log.error(messages.getMessage("err.customer.not.created", null, null), 
						customerResponse.getErrorCode().concat(" - ").concat(customerResponse.getErrorMessage()),null);
			}
			log.info("Customer successfully featched from Illion System");
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.customer.not.created", null, null), e.getMessage(),e);
		}
		return customerResponse;
	}

}
