package com.xlrs.transaction.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.transaction.dto.OrganisationUserView;
import com.xlrs.transaction.feign.OrganisationUserProxy;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class OrganisationUserRestTemplate {
	
	
	@Autowired
//	private RestServiceBuilder<ResponseView> restServiceBuilder;
	private OrganisationUserProxy organisationUserProxy;
	
	@Value("${orguserid.organisationuser.get.url}")
	private String getOrganisationUserURL;
	
	@Autowired
	private MessageSource messages;

	public OrganisationUserView getOrganisationUserByOrgUserId(Long orgUserId) throws NoSuchMessageException, ApplicationException {
		OrganisationUserView organisationUserView =null;
		try {
			if(getOrganisationUserURL == null || "".equals(getOrganisationUserURL)) {
				throw new ApplicationException(messages.getMessage("err.service.url.not.found", null, null));
			}
//			ResponseView responseView = restServiceBuilder.get(getOrganisationUserURL, ResponseView.class, orgUserId);
			ResponseView responseView = organisationUserProxy.getOrgUser(orgUserId);
			if(responseView.getErrorMessages()==null) {
				organisationUserView =  new ObjectMapper().convertValue(responseView.getData(), OrganisationUserView.class);
			}
			log.info("Organisation user successfully created");
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.service.customerdata.error.message", null, null), e.getMessage(),e);
		}
		return organisationUserView;
	}

}
