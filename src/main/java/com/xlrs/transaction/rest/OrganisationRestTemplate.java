package com.xlrs.transaction.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.transaction.feign.OrganisationSystemProxy;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class OrganisationRestTemplate {
	
	
	@Autowired
//	private RestServiceBuilder<ResponseView> restServiceBuilder;
	private OrganisationSystemProxy organisationSystemProxy;
	
//	@Value("${orgid.legalentities.get.url}")
//	private String getOrganisationURL;
	
	@Autowired
	private MessageSource messages;

	public Long[] getLegalEntitiesByOrgId(Long orgId) throws NoSuchMessageException, ApplicationException {
		Long[] legalEntityArr =null;
		try {
//			if(getOrganisationURL == null || "".equals(getOrganisationURL)) {
//				throw new ApplicationException(messages.getMessage("err.service.url.not.found", null, null));
//			}
//			ResponseView responseView = restServiceBuilder.get(getOrganisationURL, ResponseView.class, orgId);
			ResponseView responseView = organisationSystemProxy.getLegalEntityIdsByOrgId(orgId);
			if(responseView.getErrorMessages()==null) {
				legalEntityArr =  new ObjectMapper().convertValue(responseView.getData(), Long[].class);
			}
			
			log.info("Successfully fetched all legalEntities by Org Id");
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.service.customerdata.error.message", null, null), e.getMessage(),e);
		}
		return legalEntityArr;
	}
	

}
