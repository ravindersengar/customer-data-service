package com.xlrs.transaction.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.transaction.dto.BankAccountView;
import com.xlrs.transaction.feign.BankAccountsProxy;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class BankAccountRestTemplate {
	
	
	@Autowired
//	private RestServiceBuilder<ResponseView> restServiceBuilder;
	private BankAccountsProxy bankAccountsProxy;
	
	@Value("${legalentities.bankAccounts.get.url}")
	private String getBankAccountURL;
	
	@Autowired
	private MessageSource messages;

	public List<BankAccountView> getBankAccount(Long[] legalEntityIds) throws NoSuchMessageException, ApplicationException {
		List<BankAccountView> bankAccountViewArr =null;
		try {
			if(getBankAccountURL == null || "".equals(getBankAccountURL)) {
				throw new ApplicationException(messages.getMessage("err.service.url.not.found", null, null));
			}
//			ResponseView responseView = restServiceBuilder.post(getBankAccountURL, legalEntityIds, ResponseView.class);
			ResponseView responseView = bankAccountsProxy.getAllBankAccountByLegalEntityIds(legalEntityIds);
			if(responseView.getErrorMessages()==null) {
				bankAccountViewArr =  new ObjectMapper().convertValue(responseView.getData(), new TypeReference<List<BankAccountView>>(){});
				if(bankAccountViewArr==null || bankAccountViewArr.size()==0) {
					throw new NoResultFoundException(messages.getMessage("err.legalentities.bankAccounts.get.message", null, null));
				}
			}
			
			log.info("All lehelEntites By bank Account fetched seccessfully");
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.legalentities.bankAccounts.get.message", null, null), e.getMessage(),e);
		}
		return bankAccountViewArr;
	}

}
