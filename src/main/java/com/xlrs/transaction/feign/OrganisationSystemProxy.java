package com.xlrs.transaction.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.xlrs.commons.view.ResponseView;

@FeignClient(name = "organisation-service" , url = "${organisation.get.url}")
public interface OrganisationSystemProxy {
	@GetMapping("/system/{orgSystemId}/system")
	public ResponseView getOrganisationSystem(@PathVariable Long orgSystemId) throws Exception;
	
	@GetMapping("/legalentity/{orgId}/legalentities")
	public ResponseView getLegalEntityIdsByOrgId(@PathVariable Long orgId) throws Exception;
}
