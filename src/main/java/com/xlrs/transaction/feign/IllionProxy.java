package com.xlrs.transaction.feign;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.xlrs.transaction.view.CreateCustomerResponse;
import com.xlrs.transaction.view.CreateCustomerView;
import com.xlrs.transaction.view.CustomerAccountResponse;
import com.xlrs.transaction.view.CustomerAccountView;
import com.xlrs.transaction.view.CustomerDataView;
  
@FeignClient(name = "illion-customerdata-service" , url = "${illion.cusomer.data.url}")
public interface IllionProxy {
	
	@PostMapping("/create")
	public CreateCustomerResponse createCustomer(@RequestBody CreateCustomerView createCustomerView, @RequestHeader Map<String,String> headers) throws Exception;
	
	@PostMapping("/accounts")
	public CustomerAccountResponse customerAccounts(@RequestBody CustomerAccountView customerAccountView, @RequestHeader Map<String,String> headers) throws Exception;
	
	@PostMapping("/data")
	public String customerData(@RequestBody CustomerDataView customerDataView, @RequestHeader Map<String,String> headers) throws Exception;
}
