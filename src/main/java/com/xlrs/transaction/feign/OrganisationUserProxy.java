package com.xlrs.transaction.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.xlrs.commons.view.ResponseView;
  
@FeignClient(name = "user-service" , url = "${orguserid.organisationuser.get.url}")
public interface OrganisationUserProxy {
	
	@GetMapping("/{id}")
	public ResponseView getOrgUser(@PathVariable Long id) throws Exception;
	
}
