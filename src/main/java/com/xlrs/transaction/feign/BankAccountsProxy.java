package com.xlrs.transaction.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.xlrs.commons.view.ResponseView;

@FeignClient(name = "bank-service" , url = "${legalentities.bankAccounts.get.url}")
public interface BankAccountsProxy {
	
	@PostMapping("/{legalEntityIds}")
	public ResponseView getAllBankAccountByLegalEntityIds(@RequestBody Long[] legalEntityIds ) throws Exception;
	
}
