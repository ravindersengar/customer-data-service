package com.xlrs.transaction.adaptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdaptorFactory {
	@Autowired
	private SalesForceAdaptor salesForceAdaptor;
	
	public TargetSystem getAdaptor(String adaptorType) {
		if("salesforce".equalsIgnoreCase(adaptorType)) {
			return salesForceAdaptor;
		}
		return null;
	}
}
