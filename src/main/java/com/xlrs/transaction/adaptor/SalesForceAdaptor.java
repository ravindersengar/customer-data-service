package com.xlrs.transaction.adaptor;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.transaction.util.ConfigUtility;
import com.xlrs.transaction.util.HttpClient;

@Component
public class SalesForceAdaptor implements TargetSystem {

	@Autowired
	private ConfigUtility configUtil;
	
	@Autowired
	private HttpClient httpClient;
	
	@Autowired
	private MessageSource messages;
	
	@Override
	public Map<Long, String> pushData(JSONObject bankTransactionJSONReq) throws NoSuchMessageException, ApplicationException {
		//Authenticate with SalesForce
		//Push Data to SalesForce
		
		final String loginURL = configUtil.getSalesForceLoginUrl();
		String tripadealRestURL=configUtil.getProperty("salesforce.TXN_URL");
		try {
			String responseJSON = new ObjectMapper().writeValueAsString(bankTransactionJSONReq);
			String resp = httpClient.sendPostApexRest(loginURL, tripadealRestURL, responseJSON);
			ObjectMapper mapper1 = new ObjectMapper();
			String  strResp = mapper1.readValue(resp , String.class);
			
			Map<Long, String> retMap = new Gson().fromJson(
					strResp, new TypeToken<HashMap<Long, String>>() {}.getType()
				);
			
			return retMap;
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.service.salesforce.error.message", null, null), e.getMessage(), e);
		}
		
	}
	
}
