package com.xlrs.transaction.adaptor;

import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.context.NoSuchMessageException;

import com.xlrs.commons.exception.ApplicationException;

public interface TargetSystem {
	Map<Long, String> pushData(JSONObject bankTransactionJSONReq) throws NoSuchMessageException, ApplicationException;
}
