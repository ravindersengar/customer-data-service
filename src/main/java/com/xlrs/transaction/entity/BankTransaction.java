package com.xlrs.transaction.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xlrs.commons.entity.AbstractEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@JsonInclude(Include.NON_NULL)
@Table(uniqueConstraints={
	    @UniqueConstraint(columnNames = {
	    		"transactionType", 
	    		"narration",
	    		"reference", 
	    		"accountNumber",
	    		"amount", 
	    		"balance", 
	    		"transactionDate"
	    		})
	}) 
@AllArgsConstructor
@NoArgsConstructor
public class BankTransaction extends AbstractEntity{

	private static final long serialVersionUID = 2470669212882448341L;
	
	private String transactionType;
	
	@JsonIgnore
	private String narration;
	
	@JsonProperty("description")
	private String reference;
	
	private String accountNumber;
	
	private Double amount;
	
	private Double balance;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private Date transactionDate;
	
	@JsonIgnore
	private String targetSystemTransactionId;
	
	@JsonIgnore
	private Long targetSystemId;
	
	@JsonIgnore
	private Long sourceSystemId;

}
