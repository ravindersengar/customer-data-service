package com.xlrs.transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
@PropertySource({
    "classpath:messages.properties"
})
@ComponentScan(basePackages = "com.xlrs")
@EnableAutoConfiguration(exclude={MongoAutoConfiguration.class})
public class IllionDataApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(IllionDataApplication.class, args);
	}
}

