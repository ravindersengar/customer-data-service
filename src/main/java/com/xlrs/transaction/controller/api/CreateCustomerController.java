package com.xlrs.transaction.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.transaction.service.CreateCustomerService;
import com.xlrs.transaction.view.CreateCustomerView;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/customer")
@Slf4j
public class CreateCustomerController {

	@Autowired
	private CreateCustomerService createCustomerService;
	
	@Autowired
	private MessageSource messages;

	@PostMapping("/create")
	public ResponseView createCustomer(@RequestBody CreateCustomerView createCustomerView) throws Exception {
		log.debug("Requested payload is : " + createCustomerView);
		
		try {
			return new ResponseView(createCustomerService.createCustomer(createCustomerView));
		} catch (ApplicationException e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
}
