package com.xlrs.transaction.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.transaction.service.IllionUserLoginService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/illionUsers")
@Slf4j
public class IllionUserLoginController {

	@Autowired
	private IllionUserLoginService illionUserLoginService;
	
	@Autowired
	private MessageSource messages;

	@GetMapping("/all")
	public ResponseView getAllSystemUsers() throws Exception {
		log.debug("Requested getAllSystemUsers");
		try {
			return new ResponseView(illionUserLoginService.getAllSystemUsers());
		} catch (ApplicationException e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
}
