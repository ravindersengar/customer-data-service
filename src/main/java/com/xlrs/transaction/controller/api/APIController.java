package com.xlrs.transaction.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.transaction.service.APIService;

@RestController
@RequestMapping("/xlrs/txn-data")
public class APIController {

	@Autowired
	private APIService apiService;
	
	@PostMapping(value="/sf/create")
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public String addCustomerDataToSalesForce(){
		String salesforceResponse = null;
		try {
			salesforceResponse = apiService.getSalesforceToken();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return salesforceResponse;
	}
	

}