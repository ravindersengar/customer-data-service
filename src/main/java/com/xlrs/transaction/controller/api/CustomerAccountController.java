package com.xlrs.transaction.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.transaction.service.CustomerAccountService;
import com.xlrs.transaction.view.CustomerAccountView;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/customer")
@Slf4j
public class CustomerAccountController {

	@Autowired
	private CustomerAccountService customerAccountService;
	
	@Autowired
	private MessageSource messages;

	@PostMapping("/accounts")
	public ResponseView getCustomerAccounts(@RequestBody CustomerAccountView customerAccountView) throws Exception {
		log.debug("Requested payload is : " + customerAccountView);
		try {
			return new ResponseView(customerAccountService.getCustomerAccounts(customerAccountView));
		} catch (ApplicationException e) {
			throw new ApplicationException(messages.getMessage("err.configuring.customer.account", null, null), e.getMessage(),e);
		}
	}
	
}
