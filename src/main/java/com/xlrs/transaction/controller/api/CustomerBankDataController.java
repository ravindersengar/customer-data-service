package com.xlrs.transaction.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.transaction.service.CustomerBankDataService;
import com.xlrs.transaction.view.CustomerDataView;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/customer")
@Slf4j
public class CustomerBankDataController {

	@Autowired
	private CustomerBankDataService customerBankDataService;
	
	@Autowired
	private MessageSource messages;

	@PostMapping("/data")
	public ResponseView getCustomerBankData(@RequestBody CustomerDataView customerDataView) throws Exception {
		log.debug("Requested payload is : " + customerDataView);
		
		try {
			return customerBankDataService.getCustomerBankData(customerDataView);
		}catch(Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null), e.getMessage(),e);
		}
	}
	
}
