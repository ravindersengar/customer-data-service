package com.xlrs.transaction.dto;

import com.xlrs.commons.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BankAccountView implements BaseView{

	private static final long serialVersionUID = 1L;
	
	private String accountType;
	private String accountId;
	private String userCurrency;
	private String accountHolderType;
	private Long legalEntityId;
	private Long bankBranchId;
	private String targetSystemBankAccountId;
	private String bankBranchCode;
	
	private Long id;
	private String status;
}
