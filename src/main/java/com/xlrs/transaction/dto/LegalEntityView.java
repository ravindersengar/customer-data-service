package com.xlrs.transaction.dto;

import com.xlrs.commons.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LegalEntityView implements BaseView {

	private static final long serialVersionUID = 1L;
	
		private String name;
		private Long organisationId;
		private Long id;
		private String status;
}
