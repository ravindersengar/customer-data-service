package com.xlrs.transaction.dto;

import com.xlrs.commons.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IllionUserLoginView implements BaseView{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8871751311199050454L;
	
	private Long id;
	private String customerId;
	private String encryptionKey;
	private String username;
	private String password;
	private String userRef;
}
