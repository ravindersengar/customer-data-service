package com.xlrs.transaction.view;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class CustomerAccountResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	private List<AccountView> accounts;
	private String user_token;
	private String referral_code;
	private CreateCustomerResponse customer;
	private String dataRequestLink;
	
	@JsonProperty("error_code")
	private String errorCode;
	
	@JsonProperty("error_message")
	private String errorMessage;
	
	
	

}
