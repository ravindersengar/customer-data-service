package com.xlrs.transaction.view;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AccountView implements Serializable, BaseView{
	
	private static final long serialVersionUID = 3133708490786935278L;
	
	@NotEmpty(message = "{accountView.accountHolder.mandatory.feild.notempty}")
	private String accountHolder;
	@NotEmpty(message = "{accountView.name.mandatory.feild.notempty}")
	private String name;
	@NotEmpty(message = "{accountView.accountNumber.mandatory.feild.notempty}")
	private String accountNumber;
	private Long id;
	@NotEmpty(message = "{accountView.bsb.mandatory.feild.notempty}")
	private String bsb;
	@NotEmpty(message = "{accountView.balance.mandatory.feild.notempty}")
	private String balance;
	@NotEmpty(message = "{accountView.available.mandatory.feild.notempty}")
	private String available;
	@NotEmpty(message = "{accountView.accountHolderType.mandatory.feild.notempty}")
	private String accountHolderType;
	@NotEmpty(message = "{accountView.accountType.mandatory.feild.notempty}")
	private String accountType;
	
	public AccountView(String accountNumber, String bsb, String accountHolderType, String accountType) {
		super();
		this.accountNumber = accountNumber;
		this.bsb = bsb;
		this.accountHolderType = accountHolderType;
		this.accountType = accountType;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountView other = (AccountView) obj;
		if (accountHolderType == null) {
			if (other.accountHolderType != null)
				return false;
		} else if (!accountHolderType.equals(other.accountHolderType))
			return false;
		if (accountNumber == null) {
			if (other.accountNumber != null)
				return false;
		} else if (!accountNumber.equals(other.accountNumber))
			return false;
		if (accountType == null) {
			if (other.accountType != null)
				return false;
		} else if (!accountType.equals(other.accountType))
			return false;
		if (bsb == null) {
			if (other.bsb != null)
				return false;
		} else if (!bsb.equals(other.bsb))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountHolderType == null) ? 0 : accountHolderType.hashCode());
		result = prime * result + ((accountNumber == null) ? 0 : accountNumber.hashCode());
		result = prime * result + ((accountType == null) ? 0 : accountType.hashCode());
		result = prime * result + ((bsb == null) ? 0 : bsb.hashCode());
		return result;
	}

	
	
	
	
}
