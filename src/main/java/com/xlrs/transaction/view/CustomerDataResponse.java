package com.xlrs.transaction.view;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class CustomerDataResponse implements Serializable{
	
	private static final long serialVersionUID = 1640199255877836428L;
	
	private String accountHolder;
	private String name;
	private String accountNumber;
	private Long id;
	private String bsb;
	private String balance;
	private String available;
	private String interestRate;
	private String accountHolderType;
	private String accountType;
	private StatementDataResponse statementData;
	private String institution;
}
