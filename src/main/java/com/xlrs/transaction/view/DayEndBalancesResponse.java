package com.xlrs.transaction.view;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class DayEndBalancesResponse implements Serializable{

	private static final long serialVersionUID = -8509035467969522047L;
	private Date date;
	private Double balance;

}
