package com.xlrs.transaction.view;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xlrs.commons.view.BaseView;
import com.xlrs.transaction.dto.BankAccountView;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonInclude(Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDataView implements Serializable, BaseView{
	
	
	public CustomerDataView(Long orgUserId, String errorCode, String errorMessage) {
		super();
		this.orgUserId = orgUserId;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	@NotEmpty(message = "{customerDataView.customerId.mandatory.feild.notempty}")
	private String customerId;
	
	@NotEmpty(message = "{customerDataView.encryptionKey.mandatory.feild.notempty}")
	private String encryptionKey;
	
	private boolean generateRawFile;
	
	@NotEmpty(message = "{customerDataView.requestNumDays.mandatory.feild.notempty}")
	private Integer requestNumDays;
	
	private List<Long> accounts;
	
	private Map<Long, BankAccountView> accountMap;
	
	private Long orgUserId;
	
	@JsonProperty("error_code")
	private String errorCode;
	
	@JsonProperty("error_message")
	private String errorMessage;
	
	private Map<String, String> accountTargetSystemMap;
		
}
