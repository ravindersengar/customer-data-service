package com.xlrs.transaction.view;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonInclude(Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class CreateCustomerView implements Serializable, BaseView{
	
	
	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = "{createCustomerView.name.mandatory.feild.notempty}")
	private String name;
	@NotEmpty(message = "{createCustomerView.userRef.mandatory.feild.notempty}")
	private String userRef;
	@NotEmpty(message = "{createCustomerView.generatefile.mandatory.feild.notempty}")
	private String generatefile;
	private CredentialsView credentials;
	@NotEmpty(message = "{createCustomerView.orgUserId.mandatory.feild.notempty}")
	private Long orgUserId;
	@NotEmpty(message = "{createCustomerView.orgSystemId.mandatory.feild.notempty}")
	private Long orgSystemId;

}
