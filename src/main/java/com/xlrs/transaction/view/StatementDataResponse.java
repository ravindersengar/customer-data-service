package com.xlrs.transaction.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class StatementDataResponse implements Serializable{

	private static final long serialVersionUID = 2945865865788698189L;
	
	private List<DetailResponse> details;
	private Double totalCredits;
	private Double totalDebits;
	private Double openingBalance;
	private Double closingBalance;
	private Double startDate;
	private Date endDate;
	private Double minBalance;
	private Double maxBalance;
	private List<DayEndBalancesResponse> dayEndBalances;
	private Double minDayEndBalance;
	private Double maxDayEndBalance;
	private Integer daysInNegative;
	private String errorMessage;
	

}
