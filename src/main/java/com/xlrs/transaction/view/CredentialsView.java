package com.xlrs.transaction.view;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonInclude(Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class CredentialsView implements Serializable, BaseView{

	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = "{credentialsView.institution.mandatory.feild.notempty}")
	private String institution;
	@NotEmpty(message = "{credentialsView.username.mandatory.feild.notempty}")
	private String username;
	@NotEmpty(message = "{credentialsView.password.mandatory.feild.notempty}")
	private String password;
	

}
