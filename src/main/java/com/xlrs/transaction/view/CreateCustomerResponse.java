package com.xlrs.transaction.view;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class CreateCustomerResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Long systemUserId;
	private String customerId;
	private String encryptionKey;
	
	@JsonProperty("error_code")
	private String errorCode;
	
	@JsonProperty("error_message")
	private String errorMessage;
	
	private Map<String, String> accountTargetSystemMap;
	
	
	

}
