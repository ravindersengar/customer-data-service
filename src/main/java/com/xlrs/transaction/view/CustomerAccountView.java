package com.xlrs.transaction.view;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class CustomerAccountView implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = "{customerAccountView.customerId.mandatory.feild.notempty}")
	private String customerId;
	@NotEmpty(message = "{customerAccountView.encryptionKey.mandatory.feild.notempty}")
	private String encryptionKey;
	@NotEmpty(message = "{customerAccountView.orgUserId.mandatory.feild.notempty}")
	@Transient
	private Long orgUserId;

}
