package com.xlrs.transaction.view;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class DetailResponse implements Serializable{

	private static final long serialVersionUID = -7608007510146743573L;
	
	private Date date;
    private String text;
    private String notes;
    private String transactionHash;
    private String hashText;
    private Double amount;
    private String type;
    private Double balance;

}
