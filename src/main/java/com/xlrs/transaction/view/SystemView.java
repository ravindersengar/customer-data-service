package com.xlrs.transaction.view;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class SystemView implements BaseView{
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	@NotEmpty(message = "{systemView.name.mandatory.feild.notempty}")
	private String name;
	
	@NotEmpty(message = "{systemView.type.mandatory.feild.notempty}")
	private String type;
	
	@NotEmpty(message = "{systemView.code.mandatory.feild.notempty}")
	private String code;
}
