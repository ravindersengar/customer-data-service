package com.xlrs.transaction.sqlrepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xlrs.transaction.entity.SystemUser;

@Repository
public interface SystemUserRepository extends JpaRepository<SystemUser, Long>{

	SystemUser getByOrganisationUserId(Long orgUserId);

}
