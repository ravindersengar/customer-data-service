package com.xlrs.transaction.sqlrepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xlrs.transaction.entity.IllionUserLogin;

@Repository
public interface IllionUserLoginRepository extends JpaRepository<IllionUserLogin, Long>{

}
