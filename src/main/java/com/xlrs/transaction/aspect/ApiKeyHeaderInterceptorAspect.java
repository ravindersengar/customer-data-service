package com.xlrs.transaction.aspect;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.xlrs.commons.aspect.ThreadAttribute;
import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.exception.DataFormatException;

@Aspect
@Configuration
public class ApiKeyHeaderInterceptorAspect {

	@Autowired
	private MessageSource messages;
	
	private static List<String> excludedURLs = Arrays.asList("/xlrs/illionUsers/all");
	
	@Before("bean(*Controller)")
	public void before(JoinPoint joinPoint) throws DataFormatException {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		
		if(!excludedURLs.contains(request.getRequestURI())) {
			String requestHeader = request.getHeader(CommonConstants.API_KEY);
			if (requestHeader == null || requestHeader.isEmpty()) {
				throw new DataFormatException(messages.getMessage("err.bad.request", null, null));
			}else {
				ThreadAttribute.setApiKey(requestHeader);
			}
		}else {
			//do nothing as of now
		}
				
		
	}
}
