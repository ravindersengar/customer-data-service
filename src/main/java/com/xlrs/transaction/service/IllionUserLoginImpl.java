package com.xlrs.transaction.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.transaction.dto.IllionUserLoginView;
import com.xlrs.transaction.entity.IllionUserLogin;
import com.xlrs.transaction.sqlrepository.IllionUserLoginRepository;

@Service
public class IllionUserLoginImpl implements IllionUserLoginService{

	@Autowired
	private IllionUserLoginRepository illionUserLoginRepository;
	
	@Override
	public List<IllionUserLoginView> getAllSystemUsers() throws ApplicationException {
		List<IllionUserLogin>  illionUserLogins = illionUserLoginRepository.findAll();
		List<IllionUserLoginView> illionUserLoginViews = new ArrayList<IllionUserLoginView>();
		
		for(IllionUserLogin iu: illionUserLogins) {
			illionUserLoginViews.add(new IllionUserLoginView(iu.getId(), iu.getCustomerId(), iu.getEncryptionKey(), null, null, iu.getUserRef()));
		}
		return illionUserLoginViews;
	}


}
