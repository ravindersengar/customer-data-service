package com.xlrs.transaction.service.mongo;
import java.util.Date;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.xlrs.transaction.mongo.MongoDBClinent;
import com.xlrs.commons.exception.ApplicationException;

@Service
public class MongoDBServiceImpl implements MongoDBService {

	@Autowired
	private MongoDBClinent mongoDBClinent;

	@Override
	public void insert(String jsonString) throws Exception{
		try {
			MongoDatabase db = mongoDBClinent.getMongoDatabase();
			MongoCollection<Document> table = db.getCollection("customerBankDataCollection");
			Document doc = new Document("customerData",  jsonString);
			doc.append("createdDate", new Date());
			table.insertOne(doc);
		}catch(Exception ex) {
			throw new ApplicationException("MongoDB data insertion failed " + ex.getLocalizedMessage());
		}
	}


}