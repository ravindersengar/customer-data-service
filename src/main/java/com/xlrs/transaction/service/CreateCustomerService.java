package com.xlrs.transaction.service;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.transaction.view.CreateCustomerResponse;
import com.xlrs.transaction.view.CreateCustomerView;

public interface CreateCustomerService {

	public CreateCustomerResponse createCustomer(CreateCustomerView createCustomerView) throws ApplicationException;

	void saveCustomerDetails(CreateCustomerResponse customerObj)  throws ApplicationException, NoResultFoundException;;

}
