package com.xlrs.transaction.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.transaction.entity.IllionUserLogin;
import com.xlrs.transaction.entity.SystemUser;
import com.xlrs.transaction.rest.IllionCreateCustomerRestTemplate;
import com.xlrs.transaction.sqlrepository.IllionUserLoginRepository;
import com.xlrs.transaction.sqlrepository.SystemUserRepository;
import com.xlrs.transaction.view.CreateCustomerResponse;
import com.xlrs.transaction.view.CreateCustomerView;

@Service
public class CreateCustomerServiceImpl implements CreateCustomerService {

	@Autowired
	IllionCreateCustomerRestTemplate illionCreateCustomerRestTemplate;

	@Autowired
	IllionUserLoginRepository illionUserLoginRepository;
	
	@Autowired
	SystemUserRepository systemUserRepository;

	@Autowired
	private MessageSource messages;

	
	@Override
	public CreateCustomerResponse createCustomer(CreateCustomerView createCustomerView) throws ApplicationException {
		
		SystemUser systemUser = new SystemUser(createCustomerView.getOrgUserId(), createCustomerView.getName(), createCustomerView.getOrgSystemId(), createCustomerView.getOrgUserId());
		systemUser = systemUserRepository.save(systemUser);
		
		CreateCustomerResponse createCustomerResponse = illionCreateCustomerRestTemplate.createCustomer(createCustomerView);
		createCustomerResponse.setSystemUserId(systemUser.getId());
		
		if(createCustomerResponse.getErrorCode()==null) {
			
			IllionUserLogin illionUserLogin = new IllionUserLogin(systemUser.getId(), createCustomerResponse.getCustomerId(), 
					createCustomerResponse.getEncryptionKey(),createCustomerView.getCredentials().getUsername(), 
					createCustomerView.getCredentials().getPassword(), createCustomerView.getUserRef() );
			
			illionUserLogin = illionUserLoginRepository.save(illionUserLogin);
		}
		return createCustomerResponse;
		
	}

	@Override
	public void saveCustomerDetails(CreateCustomerResponse customerObj) throws ApplicationException, NoResultFoundException {
		IllionUserLogin illionUserLogin = getIllionUserLogin(customerObj);
		illionUserLogin = illionUserLoginRepository.save(setGetEncKeyAndCustId(customerObj, illionUserLogin));
	}

	private IllionUserLogin getIllionUserLogin(CreateCustomerResponse customerObj) throws NoResultFoundException {
		Optional<IllionUserLogin> optional = illionUserLoginRepository.findById(customerObj.getSystemUserId());
		if(!optional.isPresent()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		
		IllionUserLogin illionUserLogin = optional.get();
		return illionUserLogin;
	}
	
	private IllionUserLogin setGetEncKeyAndCustId(CreateCustomerResponse customerObj, IllionUserLogin illionUserLogin) {
		illionUserLogin.setEncryptionKey(customerObj.getEncryptionKey());
		illionUserLogin.setCustomerId(customerObj.getCustomerId());
		return illionUserLogin;
	}

}
