package com.xlrs.transaction.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.xlrs.transaction.util.ConfigUtility;
import com.xlrs.transaction.util.HttpClient;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class APIService {

	@Autowired
	ConfigUtility configUtility;
	
	@Autowired
	HttpClient httpClient;
	
	@Value("${salesforce.TXN_URL}")
	private String sfTxnDataPostURL;
	
	public String getSalesforceToken() throws Exception {
		final String loginURL = configUtility.getSalesForceLoginUrl();
		
		String str = "{\"xlrs__Bank_Account__c\":\"a022x00000WC6j4AAD\",\"xlrs__Transaction_Description__c\":\"For Banking Purpose\","
				+ "\"xlrs__Transaction_Date__c\":\"2021-11-09\",\"xlrs__Transaction_Amount__c\":1000,"
				+ "\"xlrs__Bank_Account_Balance__c\":10000,\"xlrs__External_System__c\":\"XLRS\"}";
		String resp = httpClient.sendPostApexRest(loginURL, sfTxnDataPostURL, str);
		log.debug(str);
		return resp;
	}
}
