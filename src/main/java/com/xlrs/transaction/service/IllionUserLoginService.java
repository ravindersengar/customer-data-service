package com.xlrs.transaction.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.transaction.dto.IllionUserLoginView;

@Service
public interface IllionUserLoginService {

	public List<IllionUserLoginView> getAllSystemUsers() throws ApplicationException;

}
