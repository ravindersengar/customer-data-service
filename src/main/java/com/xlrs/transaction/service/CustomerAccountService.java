package com.xlrs.transaction.service;

import org.json.simple.parser.ParseException;
import org.springframework.context.NoSuchMessageException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.transaction.view.CustomerAccountView;
import com.xlrs.transaction.view.CustomerDataView;

public interface CustomerAccountService {

	CustomerDataView getCustomerAccounts(CustomerAccountView customerAccountView) 
			throws ApplicationException, JsonProcessingException, ParseException, NoSuchMessageException, NoResultFoundException;

}
