package com.xlrs.transaction.service;

import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.transaction.view.CustomerDataView;

@Service
public interface CustomerBankDataService {

	public ResponseView getCustomerBankData(CustomerDataView customerDataView) 
			throws ApplicationException, JsonProcessingException, ParseException, NoResultFoundException;

}
