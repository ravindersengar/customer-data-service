package com.xlrs.transaction.service;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.transaction.entity.SystemUser;
import com.xlrs.transaction.helper.CustomerBankAccountDataHelper;
import com.xlrs.transaction.rest.IllionCustomerAccountRestTemplate;
import com.xlrs.transaction.sqlrepository.SystemUserRepository;
import com.xlrs.transaction.view.CreateCustomerResponse;
import com.xlrs.transaction.view.CustomerAccountResponse;
import com.xlrs.transaction.view.CustomerAccountView;
import com.xlrs.transaction.view.CustomerDataView;

@Service
public class CustomerAccountServiceImpl implements CustomerAccountService {

	@Autowired
	private IllionCustomerAccountRestTemplate illionCustomerAccountRestTemplate;

	@Autowired
	private MessageSource messages;

	@Autowired
	CustomerBankDataService customerBankDataService;

	@Autowired
	CreateCustomerService createCustomerService;

	@Autowired
	CustomerBankAccountDataHelper customerBankAccountDataHelper;
	
	@Autowired
	SystemUserRepository systemUserRepository;
	
	@Override
	public CustomerDataView getCustomerAccounts(CustomerAccountView customerAccountView) throws ApplicationException, JsonProcessingException, ParseException, NoResultFoundException {
	
		CustomerAccountResponse customerAccountsResponse = illionCustomerAccountRestTemplate.getCustomerAccounts(customerAccountView);
		
		if(customerAccountsResponse.getErrorCode()==null) {
			CreateCustomerResponse customerObj = customerAccountsResponse.getCustomer();
			customerObj.setSystemUserId(customerAccountView.getOrgUserId());
			createCustomerService.saveCustomerDetails(customerObj);
			
			SystemUser sysUser = systemUserRepository.getByOrganisationUserId(customerAccountView.getOrgUserId());
			if(sysUser==null) {
				throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
			}
			return customerBankAccountDataHelper.getCustomerResponseData(customerObj.getSystemUserId(), customerAccountsResponse, sysUser);
		}else {
			return new CustomerDataView(customerAccountView.getOrgUserId(),customerAccountsResponse.getErrorCode(),customerAccountsResponse.getErrorMessage());
			
		}
	}
}
