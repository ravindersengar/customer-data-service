package com.xlrs.transaction.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.transaction.adaptor.AdaptorFactory;
import com.xlrs.transaction.adaptor.SalesForceAdaptor;
import com.xlrs.transaction.entity.BankTransaction;
import com.xlrs.transaction.entity.SystemUser;
import com.xlrs.transaction.helper.CustomerDataHelper;
import com.xlrs.transaction.rest.IllionCustomerDataRestTemplate;
import com.xlrs.transaction.rest.OrganisationSystemRestTemplate;
import com.xlrs.transaction.sqlrepository.BankTransactionRepository;
import com.xlrs.transaction.sqlrepository.SystemUserRepository;
import com.xlrs.transaction.view.CreateCustomerResponse;
import com.xlrs.transaction.view.CustomerDataView;
import com.xlrs.transaction.view.SystemView;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CustomerBankDataServiceImpl implements CustomerBankDataService {

	@Autowired
	IllionCustomerDataRestTemplate illionCustomerDataRestTemplate;

	//@Autowired
	//private MongoDBService mongoDBService;

	@Autowired
	private BankTransactionRepository bankTransactionRepository;

	@Autowired
	private MessageSource messages;

	@Autowired
	CustomerDataHelper customerDataHelper;

	@Autowired
	SalesForceAdaptor salesForceAdaptor;

	@Autowired
	CreateCustomerService createCustomerService;
	
	@Autowired
	SystemUserRepository systemUserRepository;
	
	@Autowired
	private OrganisationSystemRestTemplate organisationSystemRestTemplate;
	
	@Autowired
	private AdaptorFactory adaptorFactory;
	
	@PersistenceContext
    private EntityManager em;
	
	/**
	 * AccountView to CustomerDataView Conversion will go inside cron job
	 * 
	 * @throws JsonProcessingException
	 * @throws ParseException
	 * @throws NoResultFoundException 
	 * @throws NoSuchMessageException 
	 */

	@SuppressWarnings("unchecked")
	@Override
	public ResponseView getCustomerBankData(CustomerDataView customerDataView) throws ApplicationException, JsonProcessingException, ParseException, NoSuchMessageException, NoResultFoundException {
		String customerDataResponseStr = null;
		try {
			customerDataResponseStr = illionCustomerDataRestTemplate.getCustomerData(customerDataView);
			CreateCustomerResponse customerDetailResponse = customerDataHelper.getCustomerDetailsFromCustomerResponse(customerDataView.getId(), customerDataResponseStr);
			customerDetailResponse.setSystemUserId(customerDataView.getOrgUserId());
			createCustomerService.saveCustomerDetails(customerDetailResponse);

			if (customerDataResponseStr != null && !"".equals(customerDataResponseStr)) {
				//mongoDBService.insert(customerDataResponseStr);
			}
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.service.mongo.customerbankdata.error.message", null, null), e.getMessage(), e);
		}
		
		SystemUser sysUser = systemUserRepository.getByOrganisationUserId(customerDataView.getOrgUserId());
		if(sysUser==null) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		SystemView systemView = organisationSystemRestTemplate.getSystemByOrgSystemId(sysUser.getOrganisationSystemId());
		
		List<BankTransaction> bankTransactions = null;
		
		List<BankTransaction> persistedBTs = new ArrayList<BankTransaction>();
		try {
			
			bankTransactions = customerDataHelper.getCustomerDataFromCustomerResponse(customerDataResponseStr, customerDataView.getOrgUserId(), sysUser.getOrganisationSystemId(), 1l);
			log.info("Total bank transaction fetched from illion are : " + bankTransactions.size());
			if (bankTransactions != null && bankTransactions.size() > 0) {
				for (BankTransaction bankTransaction : bankTransactions) {
					try {
						BankTransaction persistedBT = bankTransactionRepository.save(bankTransaction);
						persistedBT.setAccountNumber(customerDataView.getAccountTargetSystemMap().get(bankTransaction.getAccountNumber()));
						persistedBTs.add(persistedBT);
					} catch (ConstraintViolationException e) {
						e.getMessage();
					}catch (DataIntegrityViolationException e) {
						e.getMessage();
					}
				}
			}
			log.info("total unique records persisted are : "+ persistedBTs.size());
		
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.service.banktransaction.error.message", null, null), e.getMessage(), e);
		}
		
		JSONArray jsonArray = new JSONArray();
		jsonArray.addAll(persistedBTs);
		
		JSONObject bankTransactionJSONReq = new JSONObject();
		bankTransactionJSONReq.put("externalSourceSystem", "XLRS");
		bankTransactionJSONReq.put("transactions", jsonArray);
		
		Map<Long, String> salesforceResponse = null;
		if (systemView != null) {
			salesforceResponse = adaptorFactory.getAdaptor(systemView.getName()).pushData(bankTransactionJSONReq);
		}
		
		updateTargetSystemTransactionIds(salesforceResponse);
		return new ResponseView(bankTransactionJSONReq);
	}

	@Transactional
	private void updateTargetSystemTransactionIds(Map<Long, String> salesforceResponse) {
		if(salesforceResponse!=null) {
			Set<Long> keySet = salesforceResponse.keySet();
			TypedQuery<BankTransaction> btQuery = em.createQuery("SELECT bt from BankTransaction bt where id in (:ids)", BankTransaction.class);
			List<BankTransaction> bts = btQuery.setParameter("ids", keySet).getResultList();
		    for (BankTransaction bt : bts) {
		        bt.setTargetSystemTransactionId(salesforceResponse.get(bt.getId()));
		    }
		    bankTransactionRepository.saveAll(bts);
		}
	}

}
