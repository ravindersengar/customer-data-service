package com.xlrs.transaction.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
@Component
public class ConfigUtility {

    @Autowired
    private Environment env;

    public String getProperty(String pPropertyKey) {
        return env.getProperty(pPropertyKey);
    }
    
    public String getSalesForceLoginUrl() {
		String PASS = getProperty("salesforce.PASS");
		//String SecurityToken = getProperty("salesforce.SecurityToken");
		String USERNAME = getProperty("salesforce.USERNAME");
		String PASSWORD = PASS;
		String LOGINURL = getProperty("salesforce.LOGINURL");
		String GRANTSERVICE = getProperty("salesforce.GRANTSERVICE");
		String CLIENTID = getProperty("salesforce.CLIENTID");
		String CLIENTSECRET = getProperty("salesforce.CLIENTSECRET");
		
		final String loginURL = LOGINURL + GRANTSERVICE + "&client_id=" + CLIENTID + "&client_secret=" + CLIENTSECRET + "&username=" + USERNAME + "&password=" + PASSWORD;
		log.debug("Salesforce Final URL: " + loginURL);
		return loginURL;
	}
} 