package com.xlrs.transaction.util;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Component
public class HttpClient {
	// one instance, reuse
	private final CloseableHttpClient httpClient = HttpClients.createDefault();

	private void close() throws IOException {
		httpClient.close();
	}

	public String makeSecondCalloutForGET(String accessToken, String url) throws IOException {
		System.out.println("  access token from previous method: " + accessToken);
		HttpGet request = new HttpGet(url);

		// add request headers
		request.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
		// request.addHeader("HotelCode", "CP121");

		try (CloseableHttpClient httpClient = HttpClients.createDefault(); CloseableHttpResponse response = httpClient.execute(request)) {

			// Get HttpResponse Status
			System.out.println(response.getProtocolVersion()); // HTTP/1.1
			System.out.println(response.getStatusLine().getStatusCode()); // 200
			System.out.println(response.getStatusLine().getReasonPhrase()); // OK
			System.out.println(response.getStatusLine().toString()); // HTTP/1.1 200 OK

			HttpEntity entity = response.getEntity();
			if (entity != null) {
				// return it as a String
				String result = EntityUtils.toString(entity);
				System.out.println(result);
				return result;
			}

		} finally {
			close();
		}
		return null;
	}

	public String makeSecondCalloutForPOST(String accessToken, String url, String json) throws IOException {
		System.out.println("  access token from previous method: " + accessToken);
		HttpPost request = new HttpPost(url);
		// add request headers
		request.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
		// request.addHeader("HotelCode", "CP121");

		StringEntity params = new StringEntity(json);
		request.addHeader("content-type", "application/json");
		request.setEntity(params);

		try (CloseableHttpClient httpClient = HttpClients.createDefault(); CloseableHttpResponse response = httpClient.execute(request)) {

			// Get HttpResponse Status
			System.out.println(response.getProtocolVersion()); // HTTP/1.1
			System.out.println(response.getStatusLine().getStatusCode()); // 200
			System.out.println(response.getStatusLine().getReasonPhrase()); // OK
			System.out.println(response.getStatusLine().toString()); // HTTP/1.1 200 OK

			HttpEntity entity = response.getEntity();

			if (entity != null) {
				// return it as a String
				String result = EntityUtils.toString(entity);
				System.out.println(result);
				return result;
			}

		} finally {
			close();
		}
		return null;
	}

	public String sendGetApexRest(String loginURL, String apexrestURL, String paramsValue) throws Exception {
		HttpPost post = new HttpPost(loginURL);

		try (CloseableHttpClient httpClient = HttpClients.createDefault(); CloseableHttpResponse response = httpClient.execute(post)) {
			String getResult = EntityUtils.toString(response.getEntity());
			System.out.println(getResult);
			JsonObject convertedObject = new Gson().fromJson(getResult, JsonObject.class);
			String loginAccessToken = convertedObject.get("access_token").getAsString();
			String loginInstanceUrl = convertedObject.get("instance_url").getAsString();
			System.out.println(response.getStatusLine());
			System.out.println("Successful login : " + loginInstanceUrl + "  access token/session ID: " + loginAccessToken);

			if (loginAccessToken != null) {
				apexrestURL = apexrestURL + paramsValue;
				return makeSecondCalloutForGET(loginAccessToken, apexrestURL);
			}
		} finally {
			close();
		}
		return null;

	}

	public JsonObject getSalesforceToken(String loginURL) throws Exception {
		HttpPost post = new HttpPost(loginURL);

		try (CloseableHttpClient httpClient = HttpClients.createDefault(); CloseableHttpResponse response = httpClient.execute(post)) {
			String getResult = EntityUtils.toString(response.getEntity());
			JsonObject convertedObject = new Gson().fromJson(getResult, JsonObject.class);
//			String loginAccessToken = convertedObject.get("access_token").getAsString();
//			String loginInstanceUrl = convertedObject.get("instance_url").getAsString();
//			System.out.println("Successful login : " + loginInstanceUrl + "  access token/session ID: " + loginAccessToken);
			return convertedObject;

		} finally {
			close();
		}

	}

	public String sendPostApexRestUsingToken(String loginToken, String apexrestURL, String jsonObj) throws Exception {
		try {
			if (loginToken != null) {
				return makeSecondCalloutForPOST(loginToken, apexrestURL, jsonObj);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			close();
		}
		return null;
	}

	public String sendPostApexRest(String loginURL, String apexrestURL, String jsonObj) throws Exception {
		HttpPost post = new HttpPost(loginURL);

		try (
				CloseableHttpClient httpClient = HttpClients.createDefault(); 
				CloseableHttpResponse response = httpClient.execute(post)) {
			String getResult = EntityUtils.toString(response.getEntity());
			JsonObject convertedObject = new Gson().fromJson(getResult, JsonObject.class);
			String loginAccessToken = convertedObject.get("access_token").getAsString();
			String loginInstanceUrl = convertedObject.get("instance_url").getAsString();
			System.out.println("Successful login : " + loginInstanceUrl + "  access token/session ID: " + loginAccessToken);

			if (loginAccessToken != null) {
				return makeSecondCalloutForPOST(loginAccessToken, apexrestURL, jsonObj);
			}
		} finally {
			close();
		}
		return null;

	}

	public String sendPatchApexRest(String loginURL, String apexrestURL, String jsonObj) throws IOException {
		HttpPost post = new HttpPost(loginURL);

		try (CloseableHttpClient httpClient = HttpClients.createDefault(); CloseableHttpResponse response = httpClient.execute(post)) {
			String getResult = EntityUtils.toString(response.getEntity());
			JsonObject convertedObject = new Gson().fromJson(getResult, JsonObject.class);
			String loginAccessToken = convertedObject.get("access_token").getAsString();
			String loginInstanceUrl = convertedObject.get("instance_url").getAsString();
			System.out.println("Successful login : " + loginInstanceUrl + "  access token/session ID: " + loginAccessToken);

			if (loginAccessToken != null) {
				return makeSecondCalloutForPATCH(loginAccessToken, apexrestURL, jsonObj);
			}
		} finally {
			close();
		}
		return null;

	}

	private String makeSecondCalloutForPATCH(String accessToken, String apexrestURL, String jsonObj) throws IOException {
		System.out.println("  access token from previous method: " + accessToken);
		HttpPost request = new HttpPost(apexrestURL + "/?_HttpMethod=PATCH");
		// add request headers
		request.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
		// request.addHeader("HotelCode", "CP121");

		StringEntity params = new StringEntity(jsonObj);
		request.addHeader("content-type", "application/json;charset=UTF-8");
		request.setEntity(params);

		try (CloseableHttpClient httpClient = HttpClients.createDefault(); CloseableHttpResponse response = httpClient.execute(request)) {

			// Get HttpResponse Status
			System.out.println(response.getProtocolVersion()); // HTTP/1.1
			System.out.println(response.getStatusLine().getStatusCode()); // 200
			System.out.println(response.getStatusLine().getReasonPhrase()); // OK
			System.out.println(response.getStatusLine().toString()); // HTTP/1.1 200 OK

			HttpEntity entity = response.getEntity();

			if (entity != null) {
				// return it as a String
				String result = EntityUtils.toString(entity);
				System.out.println(result);
				return result;
			}

		} finally {
			close();
		}
		return null;
	}

}
