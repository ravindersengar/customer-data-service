package com.xlrs.transaction.mongo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

@Component
public class MongoDBClinent {
	
	@Value("${mongo.db.database}")
	private String dbname;
	
	@Value("${mongodb.uri}")
	private String connectionString;
	
	
	private MongoClient getMongoClient() {
		
        MongoClient mongoClient = MongoClients.create(connectionString);
		return mongoClient;
	}
	
	public MongoDatabase getMongoDatabase() {
		MongoDatabase db = getMongoClient().getDatabase(dbname);
		return db;
	}
}
