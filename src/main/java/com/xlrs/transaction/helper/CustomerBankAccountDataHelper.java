package com.xlrs.transaction.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.transaction.dto.BankAccountView;
import com.xlrs.transaction.dto.OrganisationUserView;
import com.xlrs.transaction.entity.SystemUser;
import com.xlrs.transaction.rest.BankAccountRestTemplate;
import com.xlrs.transaction.rest.OrganisationRestTemplate;
import com.xlrs.transaction.rest.OrganisationUserRestTemplate;
import com.xlrs.transaction.view.AccountView;
import com.xlrs.transaction.view.CreateCustomerResponse;
import com.xlrs.transaction.view.CustomerAccountResponse;
import com.xlrs.transaction.view.CustomerDataView;

@Component
public class CustomerBankAccountDataHelper {
	
	@Autowired
	private OrganisationUserRestTemplate organisationUserRestTemplate;
	@Autowired
	private OrganisationRestTemplate organisationRestTemplate;
	@Autowired
	private BankAccountRestTemplate bankAccountRestTemplate;

	public CustomerDataView getCustomerResponseData(Long id, CustomerAccountResponse customerAccountsResponse, SystemUser sysUser)
			throws ApplicationException, JsonProcessingException, ParseException {

		CustomerDataView customerDataView = new CustomerDataView();
		
		OrganisationUserView organisationUserView = organisationUserRestTemplate.getOrganisationUserByOrgUserId(sysUser.getOrganisationUserId());
		Long[] legalEntityIds = organisationRestTemplate.getLegalEntitiesByOrgId(organisationUserView.getOrganisationId());
		List<BankAccountView> bankAccounts = bankAccountRestTemplate.getBankAccount(legalEntityIds);
		if (customerAccountsResponse != null) {
			List<AccountView> accountViews = customerAccountsResponse.getAccounts();
			List<Long> accountIds = new ArrayList<>();
			Map<Long, BankAccountView> accountMap = new HashMap<>();
			// populate accountTargetSystemMap;
			Map<String, String> accountTargetSystemMap = new HashMap<String, String>();
			
			for (BankAccountView bankAccount : bankAccounts) {
				AccountView acw = new AccountView(
						bankAccount.getAccountId(), bankAccount.getBankBranchCode(),
						bankAccount.getAccountHolderType(), bankAccount.getAccountType());
				if(accountViews.indexOf(acw)>-1) {
					Long baid = accountViews.get(accountViews.indexOf(acw)).getId();
					accountIds.add(baid);
					accountMap.put(baid, bankAccount);
					accountTargetSystemMap.put(bankAccount.getAccountId(), bankAccount.getTargetSystemBankAccountId());
				}
			}
			customerDataView.setOrgUserId(sysUser.getId());
			CreateCustomerResponse ccr = customerAccountsResponse.getCustomer();
			if (ccr != null) {
				customerDataView.setId(id);
				customerDataView.setCustomerId(ccr.getCustomerId());
				customerDataView.setEncryptionKey(ccr.getEncryptionKey());
				customerDataView.setGenerateRawFile(true);
				customerDataView.setRequestNumDays(1);
				customerDataView.setAccounts(accountIds);
				if(accountMap.size()>0) {
					customerDataView.setAccountMap(accountMap);
				}
				
				customerDataView.setAccountTargetSystemMap(accountTargetSystemMap);
			}
		}
		return customerDataView;
	}

}
