package com.xlrs.transaction.helper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.entity.BaseRepository;
import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.util.DateUtil;
import com.xlrs.transaction.entity.BankTransaction;
import com.xlrs.transaction.view.CreateCustomerResponse;

@Component
public class CustomerDataHelper {
	
	@Autowired
	private BaseRepository<BankTransaction> baseRepository;

	public List<BankTransaction> getCustomerDataFromCustomerResponse(String customerDataResponseStr, Long orgUserId, Long targetSystemId, Long sourceSystemId) throws ApplicationException {
		
		try {
			List<BankTransaction> bankTransactions = new ArrayList<>();
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(customerDataResponseStr);
			JSONObject accountsJsonObject = (JSONObject) jsonObject.get("accounts");
			JSONObject bank_of_statements_JsonObject = (JSONObject) accountsJsonObject.get("bank_of_statements");
			JSONArray accountsArr = (JSONArray) bank_of_statements_JsonObject.get("accounts");
			for(int i=0; i < accountsArr.size(); i++) {
				JSONObject accountObj = (JSONObject) accountsArr.get(i);
				JSONObject statementDataJson = (JSONObject) accountObj.get("statementData");
				JSONArray bankTransactionArr = (JSONArray) statementDataJson.get("details");
				BankTransaction bt = null;
				for(int j=0; j < bankTransactionArr.size(); j++) {
					bt = new BankTransaction();
					bt.setAccountNumber(accountObj.get("accountNumber").toString());
					JSONObject transactionObj = (JSONObject) bankTransactionArr.get(j);
					bt.setTransactionType((String) transactionObj.get("type"));
					bt.setNarration((String) transactionObj.get("text"));
					bt.setReference((String) transactionObj.get("hashText"));
					bt.setAmount(Double.valueOf(transactionObj.get("amount").toString()));
					bt.setBalance(Double.valueOf(transactionObj.get("balance").toString()));
					bt.setTransactionDate(DateUtil.getDateFromStr(transactionObj.get("date").toString()));
					bt.setTargetSystemId(targetSystemId);
					bt.setSourceSystemId(sourceSystemId);
//					bt.setExternalSourceSystem("XLRS");
					baseRepository.addAuditFeilds(bt, orgUserId, CommonConstants.STATUS_ACTIVE);
					bankTransactions.add(bt);
				}
			}
			return bankTransactions;
		}catch (Exception e) {
			e.printStackTrace();
			throw new ApplicationException("Json to object mapping error: " + e.getLocalizedMessage());
		}
	}
	
	public CreateCustomerResponse getCustomerDetailsFromCustomerResponse(Long id, String customerDataResponseStr) throws ApplicationException, ParseException {
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(customerDataResponseStr);
		JSONObject customerJsonObj = (JSONObject) jsonObject.get("customer");
		
		CreateCustomerResponse response = new CreateCustomerResponse(id, (String) customerJsonObj.get("customerId"), 
				(String) customerJsonObj.get("encryptionKey"), null, null, null);
		return response;
	}
	

}
