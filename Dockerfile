FROM openjdk:8-jdk-alpine
EXPOSE 4008
VOLUME /main-app
ADD /target/illion-data-service-0.1.0.jar illion-data-service-0.1.0.jar
ENTRYPOINT ["java","-jar","illion-data-service-0.1.0.jar"]